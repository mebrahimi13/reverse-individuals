import os
import glob
import shutil

INDEX_FILE_NAME = "./data/ReverseIndividuals.csv"
RESULT_DIR_NAME = "./result"

class FastaIterator:
    def __init__(self, file):
        self.f = file
        self.lastLine = ""

    def getNext(self):
        result = self.lastLine
        self.lastLine = ""
        while True:
            line = self.f.readline()
            if line == "":
                return result
            if line[0] == '>' and result == "":
                result = result + line
            elif line[0] == '>':
                self.lastLine = line
                return result
            else :
                result = result + line

    def getNextPair(self):
        p1 = self.getNext()
        p2 = self.getNext()
        return (p1, p2)
            

class FindReverseIndivdiduals:
    def __init__(self, indexFileName, resultDirName):
        self.indexFileName = indexFileName
        self.resultDirName = resultDirName

        self.reverseIndex = {}
        self.bothFiles = {}

        self.__loadReverseIndex()
        self.__createOutputFiles()

    def __cleanDir(self, dirName):
        if os.path.isdir( dirName ):
            shutil.rmtree( dirName )
        os.makedirs( dirName )
    
    def __loadReverseIndex(self):
        f = open(self.indexFileName, 'r')
        for line in f:
            entry = line.split(',')
            key = entry[0].strip()
            val = entry[1].strip()
            self.reverseIndex[key] = int(val)
        f.close()

    def __makeFiles(self):
        for i in self.reverseIndex.values():
            f = open( self.resultDirName + "/" + str(i), 'w' )
            self.bothFiles[i] = f

        f = open( self.resultDirName + "/" + "error_no_tag", 'w' )
        self.bothFiles[-1] = f

        f = open( self.resultDirName + "/" + "error_many_tags", 'w' )
        self.bothFiles[-2] = f

    def __createOutputFiles(self):
        self.__cleanDir( self.resultDirName )
        self.__makeFiles()

    def __findReverseIndices(self, seq):
        result = []
        for index in self.reverseIndex.keys():
            if index in seq.replace('\n', ''):
                result.append( self.reverseIndex[ index ] )
        return result

    def __getIndex(self, seq):
        found = self.__findReverseIndices(seq)
        if len( found ) == 1:
            return found[0]
        elif len( found ) == 0:
            ##print("error " + str(len( found )))
            return -1
        else :
            return -2

    def terminate(self):
        for f in self.bothFiles.values():
            f.close()

    def loadFileContent( self, fileName ):
        f = open( fileName , 'r' )
        fastaIterator = FastaIterator( f )
        while True:
            orig, reverse = fastaIterator.getNextPair()
            if orig == "":
                break
            index = self.__getIndex( reverse )
            self.bothFiles[ index ].write( orig )
            self.bothFiles[ index ].write( reverse )


finder = FindReverseIndivdiduals(INDEX_FILE_NAME, RESULT_DIR_NAME)
fileNames = glob.glob("./*.fa")
for fileName in fileNames:
    print("fasta file detected: " + fileName)
    finder.loadFileContent( fileName )

finder.terminate()

if len( fileNames ) == 0:
    print("no fasta file was detected in the current working directory.")
